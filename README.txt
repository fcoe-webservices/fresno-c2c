
IMPORTANT NOTES
---------------

There are two libraries used in this platform that, at this point, need to 
be manually added to a new platform build. 

These javascript libraries power the graphs and charts in the data page.

c3js/c3
https://github.com/c3js/c3

git://github.com/c3js/c3.git



d3/d3
https://github.com/d3/d3




Since the adding of the libraries in the composer file still needs to be 
figured out, you can manually copy over the libraries folder from the current 
platform to the new platform before migratiing the site. 

1. Log in as o1.ftp
2. Navigate to the static folder
3. use the following as an example substituting the platform versions:

cp -r fresno_c2c_2.0.15/web/libraries fresno_c2c_2.0.16/web/


Think these are the version numbers for the libraries:



{
    "type": "package",
    "package": {
        "name": "c3js/c3",
        "version": "v0.7.20",
        "type": "drupal-library",
        "extra": {
            "installer-name": "c3"
        },
        "dist": {
            "url": "https://github.com/c3js/c3/releases/tag/v0.7.20",
            "type": "zip"
        }
    }
},
{
    "type": "package",
    "package": {
        "name": "d3/d3",
        "version": "v5.0.0",
        "type": "drupal-library",
        "extra": {
            "installer-name": "d3"
        },
        "dist": {
            "url": "https://github.com/d3/d3/releases/tag/v5.0.0",
            "type": "zip"
        },
    }
}